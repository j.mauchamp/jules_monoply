#include <iostream>
#include "Ccase.h"
#include "Cville.h"
#include "Cdepart.h"
#include "Cjoueur.h"
#include <time.h>

using namespace std;

int main()
{
    //tableau des cases du plateau
    Ccase* lacase[40];

    //Joueurs
    Cjoueur* lejoeur[3];

    lejoeur[0] = new Cjoueur("JULES");
    lejoeur[1] = new Cjoueur("YOUVAL");
    lejoeur[2] = new Cjoueur("ENZO");
    lejoeur[3] = new Cjoueur("MAXIME");

    //case depart
    lacase[0] = new Cdepart();
    //cases de classe Ccase
    lacase[2] = new Ccase(2);
    lacase[4] = new Ccase(4);
    lacase[5] = new Ccase(5);
    lacase[7] = new Ccase(7);
    lacase[10] = new Ccase(10);
    lacase[12] = new Ccase(12);
    lacase[15] = new Ccase(15);
    lacase[17] = new Ccase(17);
    lacase[20] = new Ccase(20);
    lacase[22] = new Ccase(22);
    lacase[25] = new Ccase(25);
    lacase[28] = new Ccase(28);
    lacase[30] = new Ccase(30);
    lacase[33] = new Ccase(33);
    lacase[35] = new Ccase(35);
    lacase[36] = new Ccase(36);
    lacase[38] = new Ccase(38);
    //Cases de classe Cville
    lacase[1] = new Cville(1, "TOHO", 60, 2);
    lacase[3] = new Cville(3, "NANKATSU", 60, 4);
    lacase[6] = new Cville(6, "MASEGAKI", 100, 6);
    lacase[8] = new Cville(8, "SHIKETSU", 100, 6);
    lacase[9] = new Cville(9, "YUEI", 120, 8);
    lacase[11] = new Cville(11, "KIRI", 140, 10);
    lacase[13] = new Cville(13, "SUNA", 140, 10);
    lacase[14] = new Cville(14, "KONOHA", 160, 12);
    lacase[16] = new Cville(16, "NEKOMA", 180, 14);
    lacase[18] = new Cville(18, "SHIRATORIZAWA", 180, 14);
    lacase[19] = new Cville(19, "KARASUNO", 200, 16);
    lacase[21] = new Cville(21, "ZABAN", 220, 18);
    lacase[23] = new Cville(23, "PEIJIN", 220, 18);
    lacase[24] = new Cville(24, "YORK SHIN", 240, 20);
    lacase[26] = new Cville(26, "MUR MARIA", 260, 22);
    lacase[27] = new Cville(27, "MUR ROSE", 260, 22);
    lacase[29] = new Cville(29, "MUR SHEENA", 280, 24);
    lacase[31] = new Cville(31, "TOKYO", 300, 26);
    lacase[32] = new Cville(32, "ROME", 300, 26);
    lacase[34] = new Cville(34, "MORIO", 320, 28);
    lacase[37] = new Cville(37, "ROUGH TELL", 350, 35);
    lacase[39] = new Cville(39, "MARIE-JOIE", 400, 50);

    //lanc� de d� + affichage case 
    string selection;
    int j = 0;

    for (int i = 0; i < 10; i++)
    {
            srand(time(NULL));
            int nbgen = rand() % 6 + 1;    //entre 1-6

            if (i < 10)
            {
                cout << "\n lancer le de ou abandonner";
                cout << "\n===========";
                cout << "\n 1 - Lancer";
                cout << "\n 2 ou autre - Abandonner";
                cout << "\n";
            
                cin >> selection;
                cout << endl;
            
                if (selection == "1")
                {
                    cout << "lancer:  " << nbgen << endl;
                    j = j + nbgen;
                    if (j > 39)
                    {
                        j = j - 40;
                        cout << j << " ";
                        lacase[j]->Aff();
                        lacase[j]->Jouer();
                    }
                    else
                    {
                        cout << j << " ";
                        lacase[j]->Aff();
                        lacase[j]->Jouer();
                    }
                }
                else if (selection == "2")
                {
                    cout << "\n Abandonner" << endl;
                    return 0;
                }
                else
                {
                    cout << "\n Abandonner" << endl;
                    return 0;
                }
            }
            else
            {
                return 0;
            }

    }

}
