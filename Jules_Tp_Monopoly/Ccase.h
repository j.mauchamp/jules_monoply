#pragma once

class Ccase
{
public:
	Ccase(int pNum = 0);
	~Ccase();
	virtual void Aff();
	virtual void Jouer();
	int GetNumCase();
	int SetNumCase(int pNum = 0);

private:
	int numCase;
};