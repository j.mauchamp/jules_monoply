#pragma once
#include "Ccase.h"

class Cville :
    public Ccase
{
public:
    Cville(int pNum, const char* nomVille, int prixVente, int loyerNu);
    ~Cville();
    void Aff();
    void Jouer();

private:
    int pNum;
    const char* nomVille;
    int prixVente;
    int loyerNu;
    bool aVendre;
};

