#include "Cville.h"
#include <iostream>
using namespace std;

Cville::Cville(int pNum, const char* nomVille, int prixVente, int loyerNu) : Ccase(pNum)
{
    this->aVendre = true;
	this->pNum = pNum;
	this->nomVille = nomVille;
	this->prixVente = prixVente;
	this->loyerNu = loyerNu;
}

Cville::~Cville()
{
}

void Cville::Aff()
{
	cout << "ville: " << nomVille << "   prix de vente: " << prixVente << "   loyer nu: " << loyerNu << endl;
}
void Cville::Jouer()
{
    string achat;
	
    if (aVendre == true)
    {
        cout << "\n acheter ou pas acheter";
        cout << "\n===========";
        cout << "\n 1 - acheter";
        cout << "\n 2 ou autre - pas acheter";
        cout << "\n";

        cin >> achat;
        cout << endl;

        
        if (achat == "1")
        {
            cout << "vous avez achete la ville" << endl;
            this->aVendre = true;
        }
        else if (achat == "2")
        {
            cout << "Vous n'avez pas achete la ville" << endl;
        }
        else
        {
            cout << "\n Mauvaise saisie" << endl;
        }
    }
    else
    {
        cout << "Ville deja achete" << endl;
    }
}